package com.ydl.iec.demo;

import com.ydl.iec.util.ByteUtil;
import com.ydl.iec.util.Iec104Util;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class SenderThreadTest {
    @Test
    public void testRegexDigital(){
        Pattern m = Pattern.compile("^(-)?\\d*(\\.\\d+)?$");
        assertTrue("匹配",m.matcher("100").matches());
        assertTrue("匹配",m.matcher("10.12").matches());
        assertTrue("匹配",m.matcher(".10").matches());
        assertTrue("匹配",m.matcher("-.10").matches());
        assertTrue("匹配",m.matcher("-2.10").matches());
        assertTrue("匹配",m.matcher("11.23").matches());
        assertFalse("不匹配",m.matcher("11.2.3").matches());
        assertFalse("不匹配",m.matcher(".").matches());

        float x = Float.parseFloat("-.10");
        float y = Float.parseFloat(".10");
        System.out.println(x);
        System.out.println(y);
    }

    @Test
    public void testHex2Dec(){
        byte[] x = ByteUtil.hexStringToBytes("4001H");
        int val = 0;
        for(byte b : x){
            val = val << 8 | b;
        }
        assertEquals(0x4001,val);
    }


    @Test
    public void testRandomFile(){
        File f = new File("D:\\model\\hello - 副本.txt");
        try {
            RandomAccessFile rafile = new RandomAccessFile(f,"r");
            byte[] data = new byte[228];
            rafile.seek(228);
            int x = rafile.read(data,0,228);
            System.out.println(x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
