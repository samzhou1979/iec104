package com.ydl.iec.util;

import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;

import static org.junit.Assert.fail;

public class ByteUtilTest {

	@Test
	public void testIntToByteArray() {
		byte[] bytes = ByteUtil.intToByteArray(60);
		System.err.println(bytes[0] + " " + bytes[1] + " " + bytes[2] + " " + bytes[3] + " ");
		System.err.println(Integer.toBinaryString(60));
	}

	@Test
	public void testShortToByteArray() {
		fail("Not yet implemented");
	}

	@Test
	public void testByteArrayToInt() {
		byte[] bytes = {-28,0,0,0};
		ByteBuffer buffer = ByteBuffer.wrap(bytes);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		System.err.println(buffer.getInt());
		int a = ByteUtil.byteArrayToInt(bytes);
		System.err.println(a);


	}

	@Test
	public void testByteArrayToShort() {
		int a = 1 << 1;
		System.err.print(a);
	}

	@Test
	public void testListToBytes() {
		fail("Not yet implemented");
	}

	@Test
	public void testDate2HByte() {

		byte[] bytes = ByteUtil.date2Hbyte(new Date());
		for (byte aByte : bytes) {
			System.out.print(aByte & 0xFF);
			System.out.print(" ");
		}

	}

	@Test
	public void testByte2HDate() {
//		byte[] datebytes = new byte[] {0x41, 0x00, 0x33, 0x33, 0x47, 0x42, 0x00};
		byte[] datebytes = new byte[] {0, 30, 33, 17, 49, 5, 24};
		Date date = ByteUtil.byte2Hdate(datebytes);
		System.err.print(date);
	}

	@Test
	public void testMain() {
		fail("Not yet implemented");
	}

	@Test
	public void testByteArrayToHexString() {
		fail("Not yet implemented");
	}

	@Test
	public void testByteArray2HexString() {
		fail("Not yet implemented");
	}

	@Test
	public void testByteArrayToFloat() {
		byte[] bytes = ByteUtil.floatToBytes(1000f);

		// 输出字节数组
		for (byte b : bytes) {
			System.out.printf("%02X ", b);
		}
	}

}
