package com.ydl.iec.util;

import com.ydl.iec.iec104.enums.QualifiersEnum;
import com.ydl.iec.iec104.enums.TypeIdentifierEnum;
import com.ydl.iec.iec104.message.MessageDetail;
import com.ydl.iec.iec104.message.MessageInfo;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 *
* @ClassName: RuleDetail104utilTest2
* @Description: TODO
* @author YDL
* @date 2020年5月13日
 */
public class RuleDetail104utilTest {

	@Test
	public void testGetIcontrol() {
		short accept = 0;
		short send = 0;
		byte[] control = Iec104Util.getIcontrol((short) accept, (short) send);
		assertEquals("发送序号不相等", accept, Iec104Util.getAccept(control));
		assertEquals("接收序号不相等", send, Iec104Util.getSend(control));


		accept = 1;
		send = 4;
		control = Iec104Util.getIcontrol((short) accept, (short) send);
		assertEquals("发送序号不相等", accept, Iec104Util.getAccept(control));
		assertEquals("接收序号不相等", send, Iec104Util.getSend(control));


		send = 32767;
		accept = send;
		control = Iec104Util.getIcontrol((short) accept, (short) send);
		assertEquals("发送序号不相等", accept, Iec104Util.getAccept(control));
		assertEquals("接收序号不相等", send, Iec104Util.getSend(control));
	}


	@Test
	public void testGetScontrol() {
		short accept = 0;
		byte[] control = Iec104Util.getScontrol(accept);
		assertEquals("接收序号不相等", accept, Iec104Util.getAccept(control));


		short getSsontrol = 32767;
		control = Iec104Util.getScontrol(getSsontrol);


		control = new byte[] {0x01, 0x00, 0x0A, 0x00 };

		assertEquals("接收序不相等", getSsontrol, Iec104Util.getAccept(control));
	}


	@Test
	public void testReturnValidate(){
		int sourceId = 2103;
		short terminalAddress = 1001;

		TypeIdentifierEnum typeIdentifierEnum = TypeIdentifierEnum.onePointTelecontrol;
		int sq = 0;
		boolean isContinuous = sq == 0 ? false : true;
		short accept = com.ydl.iec.demo.Test.RxCounter;
		short send = com.ydl.iec.demo.Test.TxCounter;
		byte[] control = Iec104Util.getIcontrol(accept, send);
		short transferReason = 7;
		// true：1 ; false ： 0
		boolean isTest = false;
		// true:0 false;1
		boolean isPN = true;

		// 消息地址 总召唤地址为0
		int messageAddress = 0;

		QualifiersEnum qualifiers = QualifiersEnum.getQualifiersEnum(TypeIdentifierEnum.onePointTelecontrol,(byte)1);
		List<MessageInfo> messages = new ArrayList<>();
		MessageInfo message = new MessageInfo();
		message.setMessageAddress(sourceId);
		byte[] info = {0,0,0,0,80};
		info[0] = (byte)sourceId;
		info[1] = (byte)(sourceId >> 8);
		info[2] = (byte)(sourceId >> 16);
		message.setMessageInfos(info);

		messages.add(message);
		MessageDetail ruleDetail104 = new MessageDetail(control, typeIdentifierEnum, isContinuous, isTest, isPN, transferReason,
				terminalAddress, messageAddress, messages, null, qualifiers);
		String result = ruleDetail104.getHexString();
		System.out.println(result);
	}


	@Test
	public void testGetSend() {
		fail("Not yet implemented");
	}
	@Test
	public void testGetUcontrol() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetControl() {
		fail("Not yet implemented");
	}

	@Test
	public void testIntToMessageAddress() {
	}



	@Test
	public void testMessageAddressToInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetChanged() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetChangedQualifiers() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetMeaageAttribute() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTransferReasonByte() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsYes() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsTets() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTransferReasonShort() {
		// 传输原因
		short transferReason = 6;
		// true：1 ; false ： 0
		boolean isTest = false;
		// true:0 false;1
		boolean isPN = true;
		byte[] values = ByteUtil.shortToByteArray(Iec104Util.getTransferReasonShort(isTest, isPN, transferReason));
		assertEquals(Iec104Util.getTransferReasonShort(values), transferReason);
		assertEquals(Iec104Util.isYes(values), isPN);
		assertEquals(Iec104Util.isTest(values), isTest);

		/*******************************************/
		// true：1 ; false ： 0
		isTest = true;
		// true:0 false;1
		isPN = false;
		values = ByteUtil.shortToByteArray(Iec104Util.getTransferReasonShort(isTest, isPN, transferReason));
		assertEquals(Iec104Util.getTransferReasonShort(values), transferReason);
		assertEquals(Iec104Util.isYes(values), isPN);
		assertEquals(Iec104Util.isTest(values), isTest);


		/*******************************************/
		transferReason = 32;
		// true：1 ; false ： 0
		isTest = true;
		// true:0 false;1
		isPN = false;
		values = ByteUtil.shortToByteArray(Iec104Util.getTransferReasonShort(isTest, isPN, transferReason));
		assertEquals(Iec104Util.getTransferReasonShort(values), transferReason);
		assertEquals(Iec104Util.isYes(values), isPN);
		assertEquals(Iec104Util.isTest(values), isTest);
	}

	@Test
	public void testGetTerminalAddressByte() {
		short terminalAddress = 1;
		byte[] terminalAddressByte =  Iec104Util.getTerminalAddressByte(terminalAddress);
		short newTerminalAddress =  Iec104Util.getTerminalAddressShort(terminalAddressByte);
		assertEquals("终端地址解析错误", terminalAddress, newTerminalAddress);
	}

}
