package com.ydl.iec.demo;

import lombok.Data;

@Data
public class PlanTestData {
    public float planCurve[] = new float[288];
    PlanTestData(){
        for(int i = 0;i<288;i++){
            if(i < 144)
                planCurve[i] = (float) (6000);
            else
                planCurve[i] = -(float) (6000);

        }
    }
}
