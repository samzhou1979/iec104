package com.ydl.iec.demo;

import lombok.Data;

import java.util.Date;
@Data
public class PlanCurveData {
    //HH:ss:mm
    public String time;
    public float power;
    public float soc;
    public float charge;
    public float discharge;
}
