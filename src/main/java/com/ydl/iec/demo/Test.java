package com.ydl.iec.demo;

import lombok.extern.slf4j.Slf4j;

import java.io.InputStream;
import java.util.Properties;
import java.util.Timer;

@Slf4j
public class Test {
    public static Properties config = new Properties();
    public static short RxCounter = 0; //receive
    public static short TxCounter = 0; //transport
    public static ResourceManage resourceManage = new ResourceManage();
    public static ViewPanel viewPanel;
    public static SlaveTest slaveTest;
    public static Timer timer = new Timer();
    public static int updateInterval = 2000;

    public static void init() {
        InputStream resourceAsStream = Test.class.getClassLoader().getResourceAsStream("iec104.properties");
        try {
            config.load(resourceAsStream);
            updateInterval = Integer.parseInt(config.getProperty("updateInterval"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        Test.init();
        resourceManage.init();
        viewPanel = new ViewPanel(resourceManage);
        slaveTest = new SlaveTest();
//        testPlanData自测数据
//        resourceManage.testPlanData(new PlanTestData());
        slaveTest.slaveTest();


//        new MasterTest().masterTest();

    }
}
