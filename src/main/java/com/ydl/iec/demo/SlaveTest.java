package com.ydl.iec.demo;

import com.ydl.iec.iec104.config.Iec104Config;
import com.ydl.iec.iec104.server.Iec104SlaveFactory;
import lombok.Getter;

public class SlaveTest {

    int port = 2404;
    @Getter
    SysDataHandler sysDataHandler = new SysDataHandler();
    public void slaveTest(){
        // 创建一个配置文件
        Iec104Config iec104Config  = new Iec104Config();
        // 指定收到多少帧就回复一个S帧
        short frameAmount = Short.parseShort(Test.config.getProperty("frame.amount"));
        iec104Config.setFrameAmountMax(frameAmount);
        // 终端地址
        short address = Short.parseShort(Test.config.getProperty("terminal.id"));

        iec104Config.setTerminnalAddress(address);
        try {
            Iec104SlaveFactory.createTcpServerSlave(port)
                    .setDataHandler(sysDataHandler).setConfig(iec104Config).run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
