package com.ydl.iec.demo;

import lombok.Data;

@Data
public class Point {
    /** 点号（地址） */
    private Integer pointId;
    private String pointName;
    private float value;
    /** 1遥信 2遥测 3遥控 4遥调 */
    private int pointType;
}
