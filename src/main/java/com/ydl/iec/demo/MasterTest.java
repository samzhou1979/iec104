package com.ydl.iec.demo;

import com.ydl.iec.iec104.config.Iec104Config;
import com.ydl.iec.iec104.server.Iec104MasterFactory;

public class MasterTest {

    String host = "127.0.0.1";
    int port = 2404;
    public void masterTest(){
        // 创建一个配置文件
        Iec104Config iec104Config  = new Iec104Config();
        // 指定收到多少帧就回复一个S帧
        iec104Config.setFrameAmountMax((short) 2);
        // 终端地址
        iec104Config.setTerminnalAddress((short) 1);
        try {
            Iec104MasterFactory.createTcpClientMaster(host,port)
                    .setDataHandler(new SysDataHandler()).setConfig(iec104Config).run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
