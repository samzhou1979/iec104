package com.ydl.iec.demo;


public class PlanCurve {

    private static PlanCurve instance = new PlanCurve();

    public float[] getPlanCurve() {
        return planCurve;
    }

    public float[] planCurve = new float[288];
    public float[] planCurve1 = new float[288];
    public float[] planCurve2 = new float[288];
    public float[] planCurve3 = new float[288];
    public float[] planCurve4 = new float[288];
    public float[] soc1 = new float[288];
    public float[] soc2 = new float[288];
    public float[] soc3 = new float[288];
    public float[] soc4 = new float[288];

    private PlanCurve() {
        for (int i = 0; i < 288; i++) {
            planCurve[i] = 0;
            planCurve1[i] = 0;
            planCurve2[i] = 0;
            planCurve3[i] = 0;
            planCurve4[i] = 0;
            soc1[i] = 0;
            soc2[i] = 0;
            soc3[i] = 0;
            soc4[i] = 0;
        }
    }

    public static PlanCurve getInstance() {
        return instance;
    }


}
