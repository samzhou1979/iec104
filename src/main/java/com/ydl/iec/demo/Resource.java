package com.ydl.iec.demo;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Resource {
    private String resourceId;
    private String resourceName;
    private Map<Integer, Point> points = new HashMap<>();
    /** 点号 */
    private Integer ap;
    private Integer soc;
    private Integer soh;
    private Integer charge;
    private Integer discharge;
    private Integer maxCharge;
    private Integer maxDischarge;
    private Integer state;
    private Integer controlSwitch;
    private Integer warn;

    public List<PlanCurveData> planCurveDataList = new ArrayList<>();
    //运行功率倍数
    private float ratio = 1;
    private int index = 0;

    public void setPlan(float[] planCurve, float totalCharge, float totalDischarge) {
        planCurveDataList.clear();
        String startTime = LocalDate.now()+"T00:00:00";;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:00");
        LocalDateTime parse = LocalDateTime.parse(startTime);
        float maxCharge = points.get(this.maxCharge).getValue();
        float maxDischarge = points.get(this.maxDischarge).getValue();
        for(int i = 0; i < planCurve.length; i++) {
            LocalDateTime next = parse.plusMinutes(5 * i);
            PlanCurveData previous = null;
            if(i>0){
                previous = planCurveDataList.get(i-1);
            }
            PlanCurveData planCurveData = new PlanCurveData();
            planCurveData.setTime(next.format(formatter));
            if(planCurve[i]>0) {
                planCurveData.setPower(planCurve[i] * (maxCharge/totalCharge));
                planCurveData.setSoc(previous!=null?previous.getSoc()+0.1f:points.get(soc).getValue());
                planCurveData.setCharge(previous!=null?previous.getCharge()-1f:points.get(charge).getValue());
                planCurveData.setDischarge(previous!=null?previous.getDischarge()+1f:points.get(discharge).getValue());
            }else if(planCurve[i]<0){
                planCurveData.setPower(planCurve[i] * (maxDischarge/totalDischarge));
                planCurveData.setSoc(previous!=null?previous.getSoc()-0.1f:points.get(soc).getValue());
                planCurveData.setCharge(previous!=null?previous.getCharge()+1f:points.get(charge).getValue());
                planCurveData.setDischarge(previous!=null?previous.getDischarge()-1f:points.get(discharge).getValue());
            }
            this.planCurveDataList.add(planCurveData);
        }
//        System.out.println(this.planCurveDataList);
    }

}
