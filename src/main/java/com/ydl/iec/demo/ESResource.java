package com.ydl.iec.demo;

public class ESResource {
    public float ap;
    public float soc;
    public float soh;
    public float charge;
    public float discharge;
    public float maxCharge;
    public float maxDischarge;
    public int info;
    public int state;
}
