package com.ydl.iec.iec104.server.handler;

import com.ydl.iec.demo.Test;
import com.ydl.iec.iec104.message.MessageDetail;
import com.ydl.iec.util.ByteUtil;
import com.ydl.iec.util.Iec104Util;
import io.netty.channel.ChannelHandlerContext;

/**
 *
 * @ClassName:  ChannelHandlerImpl
 * @Description: 实现一个自定义发现消息的类
 * @author: YDL
 * @date:   2020年5月19日 上午11:47:16
 */
public class ChannelHandlerImpl implements  ChannelHandler {

	private ChannelHandlerContext ctx;

	public ChannelHandlerImpl(ChannelHandlerContext ctx) {
		this.ctx = ctx;
	}

	@Override
	public void writeAndFlush(MessageDetail ruleDetail104) {
		ctx.channel().writeAndFlush(ruleDetail104);
	}

	@Override
	public void writeAndFlush(String hexString) {
//		byte[] x = Iec104Util.getScontrol(Test.RxCounter);
//		byte[] y = {104,4,0,0,0,0};
//		for(int i = 0; i < 4; i++){
//			y[i + 2] = x[i];
//		}
//		ctx.channel().writeAndFlush(y);
		ctx.channel().writeAndFlush(hexString);
	}
}
