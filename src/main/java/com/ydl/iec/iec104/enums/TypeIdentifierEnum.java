package com.ydl.iec.iec104.enums;

import lombok.Getter;

/**
 *
* @ClassName: TypeIdentifierEnum   类型标识
* @author YDL
* @date 2020年5月13日
 */
public enum TypeIdentifierEnum {

	/**
	 * 单点摇信
	 */
	onePointTeleindication(0x01, 1),
	/**
	 * 双点摇信
	 */
	twoPointTeleindication(0x03, 1),
	/**
	 * 测量值 归一化值 遥测
	 */
	normalizedTelemetry(0x09,2),
	/**
	 * 测量值  标度化值 遥测
	 */
	scaledTelemetry(0x0B, 2),
	/**
	 * 测量值 短浮点数 遥测   Short floating point
	 */
	shortFloatingPointTelemetry(0x0D, 2),
	shortFloatingPointTimeTelemetry(0x24, 2),
	/**
	 * 摇信带时标 单点
	 */
	onePointTimeTeleindication(0x1E, 1),
	/**
	 * 摇信带时标 双点
	 */
	twoPointTimeTeleindication(0x1F, 1),
	/**
	 * 单命令 遥控
	 */
	onePointTelecontrol(0x2D, 1),
	/**
	 * 双命令遥控
	 */
	// todo: Dong this is new type
	twoPointTelecontrol(0x2E, 1),

	/**
	 * 召唤全电度
	 */
	generalCallMeter(0x65, 0),
	/**
	 * 读单个参数
	 */
	readOneParameter(0x66, 4),
	/**
	 * 读多个参数
	 */
	readMultipleParameter(0x84, 4),

	/**
	 * 预置单个参数命令
	 */
	prefabActivationOneParameter(0x30, 4),
	/**
	 * 预置多个个参数
	 */
	prefabActivationMultipleParameter(0x88, 4),
	/**
	 * 初始化结束
	 */
	initEnd(0x46, 0),
	/**
	 * 召唤命令
	 */
	generalCall(0x64, 0),
	/**
	 * 时钟同步
	 */
	timeSynchronization(0x67, 0),
	/**
	 * 复位进程
	 */
	resetPprocess(0x69, 0),

	/**
	 * 测量值，short float time value
	 */
	shortFloatingTimeValue(36, 2),

	/**
	 * 测量值 短浮点数 短时标   Short floating point
	 */
	shortFloatingTimeTelemetry(0x0E, 0),

	/**
	 * 计划曲线
	 */
	// todo: ZSA this is new type
	planCurve(0x89, 4),
	/**
	 * 文件服务器
	 */
	// 文件目录召唤类型
	directoryCall(0xD2,32),
	test(104, 0);

	@Getter
	private byte value;
	@Getter
	private int messageLength;

	TypeIdentifierEnum(int value, int messageLength) {
		this.value = (byte) value;
		this.messageLength = messageLength;
	}
	public static TypeIdentifierEnum getTypeIdentifierEnum(byte value) {
		for (TypeIdentifierEnum type : TypeIdentifierEnum.values()) {
			if (type.getValue() == value) {
				return type;
			}
		}
		return null;
	}


	public static boolean isTelemetry(TypeIdentifierEnum typeIdentifierEnum) {
		return TypeIdentifierEnum.normalizedTelemetry == typeIdentifierEnum
				|| shortFloatingPointTelemetry == typeIdentifierEnum;
	}
}
