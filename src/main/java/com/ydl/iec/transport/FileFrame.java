package com.ydl.iec.transport;

import lombok.Data;

@Data
public class FileFrame {
    //APCI 内容
    private final byte FLAG = 104; //104标志
    private byte apduLen = 4; // apdu长度
    private short rxCount = 0; //接收数量
    private short txCount = 0; //发送数量

    //ASDU 内容，这里特指文件，不包含其它
    private byte type = (byte)0xD2; //1. 类型标识
    private byte vsq = 0; //2. 可变帧长限定词VSQ，本代码中改位无效
    private short cot = 5; //3. 传送原因
    private short address = 1; //ASDU公共地址

    //信息对象
    private int register = 0;// 1. 信息对象地址，附加数据包类型（合并为一个，地址3个字节，值为0，附加数据包类型：2，文件传输，1、3、4备用）
    private byte attachType = 2;
    byte op = 1; //
    private AttachData attach = null; //附加数据包对象
}