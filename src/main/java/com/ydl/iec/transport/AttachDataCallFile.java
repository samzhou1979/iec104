package com.ydl.iec.transport;

import lombok.Data;

@Data
public class AttachDataCallFile extends AttachData{
    private int fileNameLen = 0;
    private String fileName = null;
}
