package com.ydl.iec.transport;

import com.ydl.iec.iec104.config.Iec104Config;
import com.ydl.iec.iec104.server.Iec104SlaveFactory;

public class FileTransport104Test {
    public static void main(String[] args) {
        FileTransport104Test test = new FileTransport104Test();
        FileTransportHandler handler = new FileTransportHandler();
        test.fileTransportService(handler);
    }

    private void fileTransportService(FileTransportHandler handler){
        // 创建一个配置文件
        short frameCount = 0x2;
        // 终端地址
        short address = 0x01;
        int port = 2404; // 服务端口
        Iec104Config iec104Config  = new Iec104Config();
        iec104Config.setFrameAmountMax(frameCount);
        iec104Config.setTerminnalAddress(address);
        try {
            Iec104SlaveFactory.createTcpServerSlave(port)
                    .setDataHandler(handler).setConfig(iec104Config).run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
