package com.ydl.iec.transport;

import lombok.Data;

@Data
public class AttachFile {
    private String fileName = null;
    private int attribute = 0;
    private int size = 0;
    private byte[] cp56time = null;
}
