package com.ydl.iec.transport;

import lombok.Getter;

public enum AttachTypeEnum {
    DIRCALL((byte)1),DIRCFM((byte)2),
    FILECALL((byte)3),FILECFM((byte)4),
    FILETRAN((byte)5),FILETRANCFM((byte)6);//

    @Getter
    private byte type;
    AttachTypeEnum(byte val){
        this.type = val;
    }
    public static AttachTypeEnum valOf(byte type){
        AttachTypeEnum ret = DIRCALL;
        for (AttachTypeEnum val : AttachTypeEnum.values()) {
            if(val.getType() == type){
                ret = val;
                break;
            }
        }
        return ret;
    }
}
