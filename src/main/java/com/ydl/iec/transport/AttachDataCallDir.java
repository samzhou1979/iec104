package com.ydl.iec.transport;

import lombok.Data;

import java.util.Date;

@Data
public class AttachDataCallDir extends AttachData{
    String name; //文件名称
    int flag; //召唤标志：1:满足搜索条件的文件，0所有文件
    Date startTime; //查询的起始时间
    Date endTime; //查询的结束时间
}
