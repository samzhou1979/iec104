package com.ydl.iec.transport;

import lombok.Data;

import java.io.File;

@Data
public class AttachDataReplyTrans extends AttachData{
    private File transFile;
    private int segment;
}
