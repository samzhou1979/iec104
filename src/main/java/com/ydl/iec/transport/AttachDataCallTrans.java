package com.ydl.iec.transport;

import lombok.Data;

@Data
public class AttachDataCallTrans extends AttachData{
    private int segment;
    private int more;
}
