package com.ydl.iec.transport;

import lombok.Data;

import java.util.List;

@Data
public class AttachDataReplyDir extends AttachData{
    private int success = 0;
    private int more = 0;
    private int amount = 0;
    List<AttachFile> AttachFiles = null;
}
