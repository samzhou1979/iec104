package com.ydl.iec.transport;

import lombok.Data;

@Data
public class AttachDataReplyFile extends AttachData{
    int success;
    String fileName;
    int size;
}
