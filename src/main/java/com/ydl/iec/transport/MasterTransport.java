package com.ydl.iec.transport;

import com.ydl.iec.iec104.config.Iec104Config;
import com.ydl.iec.iec104.server.Iec104MasterFactory;

public class MasterTransport {
    public static void main(String[] args) throws Exception {
        MasterTransport test = new MasterTransport();
        MasterHandler handler = new MasterHandler();
        test.masterService(handler);
    }

    private void masterService(MasterHandler handler) throws Exception {
        Iec104Config iec104Config  = new Iec104Config();
        iec104Config.setFrameAmountMax((short) 2);
        iec104Config.setTerminnalAddress((short) 1);
        Iec104MasterFactory.createTcpClientMaster("127.0.0.1", 2404)
                .setDataHandler(handler).setConfig(iec104Config).run();
    }
}
